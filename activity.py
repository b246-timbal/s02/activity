year = int(input("Enter year: "))

if year <= 0:
    print("Invalid year! Please input valid year")
elif (year % 400 == 0) or (year % 100 != 0) and (year % 4 == 0):
    print("Leap Year")
else:
    print("Not a leap year")

row = int(input("Enter number for row: "))
col = int(input("Enter number for col: "))

for i in range(row):
    row_str = ""
    for j in range(col):
        row_str += "* "
    print(row_str)